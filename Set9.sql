--Set 9
--DDL operations: Drop the "projects" table.
DROP TABLE projects;

--Single Function (Number): Display the commission earned by each employee in the "employees" table,
--assuming their commission_pct represents the portion of their salary they receive as a commission.
SELECT
    first_name,
    last_name,
    salary * commission_pct AS salary_1
FROM
    employees;

--Subqueries: Write a query to find all employees whose manager earns more than the average salary of
--all managers in the "employees" table. 
SELECT
    e.first_name,
    e.last_name
FROM
         employees e
    JOIN employees m ON e.manager_id = m.employee_id
WHERE
    e.salary > (
        SELECT
            AVG(salary)
        FROM
            employees
        GROUP BY
            m.manager_id
        ORDER BY
            AVG(m.salary) DESC
        FETCH NEXT 1 ROW ONLY
    );

--Set Operators: Use the INTERSECT operator to find all department_ids that are present in both the
--"employees" and "departments" tables.
SELECT
    department_id
FROM
    employees
INTERSECT
SELECT
    department_id
FROM
    departments;

--Joins: Write a query that joins the "employees" table and the "departments" table and displays the
--employee's first name, last name, and department name, only for those departments that have more than
--50 employees.
SELECT
    first_name,
    last_name,
    department_name
FROM
         employees e
    JOIN departments d ON e.department_id = d.department_id
WHERE department_name IN (
    SELECT
        department_name
    FROM
             employees e
        JOIN departments d ON e.department_id = d.department_id
    GROUP BY
        department_name
    HAVING
        COUNT(employee_id) > 50
);
