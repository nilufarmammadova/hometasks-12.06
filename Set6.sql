--Set 6
--Aggregate Functions: Find the mode of the salary (most frequently occurring salary) in the "employees" table.
SELECT
    STATS_MODE(salary)
FROM
    employees;
    
--Subqueries: Display all employees who report to the manager who manages the most employees in the
--"employees" table.
SELECT
    *
FROM
    employees where manager_id in (
    SELECT
        manager_id
    FROM
        employees
    GROUP BY
        manager_id
    ORDER BY
        COUNT(*) DESC
    FETCH NEXT 1 ROW ONLY
);

--Single Function (Character): Use the CONCAT function to display each employee's full name
--(first name and last name) in the "employees" table.
SELECT
    concat(concat(first_name, ' '), last_name) "Full Name"
FROM
    employees;
    
--Joins: Write a query to display the first_name, last_name, and department_name of all employees
--who work in countries that have a country_name starting with 'U'. 
SELECT
    first_name,
    last_name,
    department_name,
    c.country_id,
    country_name
FROM
         employees e
    JOIN departments d ON e.department_id = e.department_id
    JOIN locations   l ON l.location_id = d.location_id
    JOIN countries   c ON c.country_id = l.country_id
where country_name like 'U%'; 


--DDL operations: Add a "country_id" column to the "employee_projects" table.
ALTER TABLE employee_projects ADD country_id NUMBER;
