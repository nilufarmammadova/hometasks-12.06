--Set 8
--SELECT statement: Select all fields from the "employees" table for employees whose first name contains
--the letter 'e' at least twice. 
SELECT
    *
FROM
    employees where first_name like '%e%e%';
    
--Joins: Write a query that RIGHT JOINs the "employees" table with the "departments" table and displays
--the employee's first name, last name, and department name. 
SELECT
    first_name,
    last_name,
    department_name
FROM
    employees   e
    RIGHT JOIN departments d ON e.department_id = d.department_id;
    
--DDL operations: Rename the "employee_projects" table to "projects".
RENAME TABLE employee_projects to projects; 

--Aggregate Functions: For each job_id in the "employees" table, find the difference between the maximum
--and minimum salaries. 
SELECT
    MAX(salary) - min(salary) difference,
    job_id
FROM
    employees
GROUP BY
    job_id;

--DML operations (INSERT): Insert a new record into the "projects" table. Use your own discretion for the
--values. 
INSERT INTO projects (project_name) VALUES ( 'Skills' );
commit;
