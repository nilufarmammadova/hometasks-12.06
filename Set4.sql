--Set 4
--DDL operations: Alter the "employees_backup" table by dropping the "performance_score" column.
ALTER TABLE employees_backup DROP COLUMN performance_score; 

--DML operations (INSERT): Insert a new record into the "employees_backup" table.
--Use your own discretion for the values.
Insert Into employees_backup (first_name, last_name, email) VALUES ( 'Nilay',
'Ahmadova',
'nlyahmdv@gmail.com' );
commit;

--Single Function (Number): Use the ROUND function to round the salary of all employees to the
--nearest hundred in the "employees" table.
SELECT
    salary,
    round(salary, - 2)
FROM
    employees;
    
--Aggregate Functions: Find the department with the highest number of employees in the "employees" table.
SELECT
    MAX(COUNT(employee_id))
FROM
    employees
GROUP BY
    department_id;

--Conversion Functions: Display the hire_date of all employees in the "employees" table
--in the format 'YYYY-Q' where Q represents the quarter of the year.
SELECT
    to_char(hire_date, 'YYYY-Q')
FROM
    employees;
