--Set 1
--DDL operations: Create a copy of the "employees" table named "employees_backup" without including the data.
CREATE TABLE employees_backup
    AS
        SELECT
            *
        FROM
            employees
        WHERE
            1 = 2;

--DML operations (UPDATE): Update the salary of all employees in the "employees_backup" table by adding
--a 15% raise for those who were hired in the year 2000.
UPDATE employees_backup
SET
    salary = salary * 1.15
WHERE
    EXTRACT(YEAR FROM hire_date) = 2000;

commit;

--Single Function (Date): List all employees in the "employees" table who were hired on a Friday.
SELECT
    *
FROM
    employees
WHERE
    to_char(hire_date, 'D') = 5;

--Aggregate Functions: Calculate the total salary paid per job in the "employees" table.
SELECT
    job_id,
    SUM(salary)
FROM
    employees
GROUP BY
    job_id;

--Joins: Using a LEFT JOIN, list all departments from the "departments" table and the count of employees
--in each department from the "employees" table.
SELECT
    department_name,
    COUNT(employee_id)
FROM
    departments d
    LEFT JOIN employees   e ON d.department_id = e.department_id
GROUP BY
    department_name;
