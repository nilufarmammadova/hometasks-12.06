--Set 2
--Conversion Functions: Show the job_id in the "jobs" table in all uppercase.
SELECT
    upper(job_id)
FROM
    jobs;
    
--Subqueries: Find all employees in the "employees" table whose salary is above the average salary
--of their respective departments.
SELECT
    *
FROM
    employees e
WHERE
    salary > (
        SELECT
            AVG(salary)
        FROM
            employees e2
        WHERE
            e2.department_id = e.department_id
            GROUP BY department_id
    );

--Set Operators: Use the UNION operator to create a list of all unique job_ids from both the "employees"
--and "job_history" tables. 
SELECT
    job_id
FROM
    employees
UNION
SELECT
    job_id
FROM
    job_history;
    
--DML operations (DELETE): Delete all records from the "employees_backup" table where the salary
--is below 3000.
DELETE FROM employees_backup
WHERE
    salary < 3000;
commit;

--DDL operations: Add a new column to the "employees_backup" table called "performance_score" of type INTEGER.
ALTER TABLE employees_backup ADD performance_score NUMBER;
