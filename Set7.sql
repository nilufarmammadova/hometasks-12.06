--Set 7
--DML operations (DELETE): Delete all employees in the "employees_backup" table who were hired in the
--second half of the year (July to December). 
DELETE FROM employees_backup
WHERE
    hire_date BETWEEN '01-07-2003' AND '31-12-2003';
    
--Set Operators: Use the UNION ALL operator to list all country_id values from both the "locations"
--and "countries" tables. 
SELECT
    country_id
FROM
    locations
UNION ALL
SELECT
    country_id
FROM
    countries;
    
--Single Function (Analytical): Display the dense rank of employees based on salary within their
--department in the "employees" table.


--Conversion Functions: Display the length of service for each employee in the "employees" table in years
--and months.
select * from jobs;

--Subqueries: Find the job title of the employee who earns the second highest salary in the "employees" table.
SELECT
    e.*,
    j.job_title
FROM
    employees e
join jobs j on e.job_id = j.job_id
WHERE
    e.salary = (
        SELECT
            salary
        FROM
            employees
        ORDER BY
            salary DESC
        OFFSET 1 ROW FETCH NEXT 1 ROW ONLY
    );
