--Set 5
--Set Operators: Use the MINUS operator to display all employees in the "employees" table
--who have never been a manager (based on the manager_id column). 
SELECT
    employee_id
FROM
    employees
MINUS
SELECT
    manager_id
FROM
    employees;

--Joins: Write a query that INNER JOINs the "employees" table with the "jobs" table and displays
--the employee's first name, last name, and job title.
SELECT
    first_name,
    last_name,
    job_title
FROM
         employees e
    JOIN jobs j ON e.job_id = j.job_id;

--SELECT statement: Select the first_name, last_name, salary, and department_id from the "employees" table
--where the salary is not in the top 5 salaries.
SELECT
    *
FROM
    employees where employee_id not in (
    SELECT
        employee_id
    FROM
        employees
    ORDER BY
        salary DESC
    FETCH NEXT 5 ROWS ONLY
);

--DDL operations: Create a new table called "employee_projects" that includes the following columns:
--"project_id" (integer), "employee_id" (integer), "start_date" (date), "end_date" (date), "role" (varchar).
CREATE TABLE employee_projects (
    project_id  NUMBER,
    employee_id NUMBER,
    start_date  DATE,
    end_date    DATE,
    role        VARCHAR2(50)
);

--DML operations (UPDATE): In the "employees_backup" table, set the commission_pct to 0 for
--all employees who don't have a commission_pct.
UPDATE employees_backup
SET
    commission_pct = 0
where
    commission_pct is NULL;
commit;
