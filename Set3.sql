--Set 3
--SELECT statement: Select the last_name, job_id, and department_id from the "employees" table
--where the last_name like 'S%'.
SELECT
    last_name,
    job_id,
    department_id
FROM
    employees
where last_name like 'S%';

--Single Function (Analytical): Use the RANK() function to display employees' salary ranking within
--their department.


--Aggregate Functions: Find the highest, lowest, and average number of employees in each job from the
--"employees" table.
SELECT
    MAX(COUNT (employee_id)),
    MIN(COUNT (employee_id)),
    AVG(COUNT (employee_id))
FROM
    employees
GROUP BY
    job_id;
    
    
--Joins: Write a query that FULL OUTER JOINs the "employees" table with the "departments" table and
--displays the employee's first name, last name, and department name.
SELECT
    first_name,
    last_name,
    department_name
FROM
    employees   e
    FULL JOIN departments d ON e.department_id = d.department_id;
    
--Subqueries: Write a query that selects all employees in the "employees" table who work in a department
--with more than 80 employees. 
SELECT
    *
FROM
    employees e
JOIN (
    SELECT
        COUNT(*) AS cnt,
        department_id
    FROM
        employees
    GROUP BY
        department_id
) e1 on e.department_id = e1.department_id
where e1.cnt > 80;
