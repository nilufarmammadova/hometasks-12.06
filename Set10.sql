--Set 10
--DML operations (UPDATE): Update the "employees_backup" table by giving a 10% salary increase to those
--employees whose job_id is 'SA_REP' and who were hired after the year 2005.
UPDATE employees_backup
SET
    salary = salary * 1.1
WHERE
        job_id = 'SA_REP'
    AND EXTRACT(YEAR FROM hire_date) > 2005;
commit;

--Aggregate Functions: Find the median salary for the 'IT_PROG' job in the "employees" table.
SELECT
    median(salary)
FROM
    employees
WHERE
    job_id = 'IT_PROG';

--Single Function (Date): List the employees in the "employees" table who have been hired in the last 365 days.
SELECT
    *
FROM
    employees
WHERE
        hire_date >= sysdate - 365
    AND hire_date < sysdate;


--Conversion Functions: Convert the hire_date in the "employees" table to 'Day, DD-Month-YYYY' format.
SELECT
    to_char(hire_date, 'Day, DD-Month-YYYY')
FROM
    employees;

--SELECT statement: Display the employees' last names and the length of the names from the "employees" table.
--Order the result by the length of the names in descending order.
SELECT
    last_name,
    length(first_name) AS "length_names"
FROM
    employees
ORDER BY
    "length_names" DESC;
